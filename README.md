# WeatherApp

This is simple single page application that presents a list of 5 hardcoded European cities to display current weather information and hourly forecast for the next 12 hours on button click.

# Architecture

Architecture of the project is really simple. The application is split into three modules: 

* App Module is the bootstrapping module
* Core Module is containing all the core components of the projects that should be protected and only initialized once
* Shared Module is containing all the core components of the projects that should be shared and initialized more then once

Starting point of the application is main.component.ts. This component is responsible for fetching all the initial data and preparing the view.
For displaying basic weather information there is city-card.components.ts
For displaying hourly weather information there is hourly-dialog.component.ts what is used with MatDialog to show popup with chart-wrapper.component.ts to display ng2-chart data.
# Technologies

* Angular 11
* Angular Material - [https://material.angular.io/](https://material.angular.io/)
* ChartJs - [https://www.chartjs.org/](https://www.chartjs.org/)
* ng2-charts - [https://valor-software.com/ng2-charts/](https://valor-software.com/ng2-charts/)

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.2.8.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.
