export interface CityWeatherData {
    name: string;
    coord: {
        lon: number,
        lat: number
    };
    main: {
        temp: number
    };
    wind: {
        speed: number
    };
    icon: string;
}

export interface CityHourlyForecast {
    dt: string;
    temp: number;
}
