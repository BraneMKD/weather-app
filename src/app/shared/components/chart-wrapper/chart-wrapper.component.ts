import { Component, Input, OnInit } from '@angular/core';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { Color, Label } from 'ng2-charts';
import { CityHourlyForecast } from '../../interfaces/weather-data';

/**
 * Chart wrapper component used to wrap external ng2-chart module - Line Chart
 */
@Component({
  selector: 'app-chart-wrapper',
  templateUrl: './chart-wrapper.component.html',
  styleUrls: ['./chart-wrapper.component.scss']
})
export class ChartWrapperComponent implements OnInit {

  @Input() weatherData!: CityHourlyForecast[];    // Weather data used to display info

  public lineChartData: ChartDataSets[] = [];     // LineChart data
  public lineChartLabels: Label[] = [];           // LineChart labels

  public lineChartOptions = {                     // LineChart options
    responsive: true,
  };

  public lineChartColors: Color[] = [             // LineChart colors
    {
      borderColor: '#046f94',
      backgroundColor: '#3493ad',
    },
  ];

  public lineChartLegend = true;                  // LineChart boolean toggle for legend
  public lineChartPlugins = [];                   // LineChart external plugins

  constructor() {}

  ngOnInit(): void {
    this.prepareDataForDisplay();
  }

  /**
   * Function to prepare fetched data to data that can be used to display line chart
   */
  private prepareDataForDisplay(): void {
    const temps: number[] = [];
    const labels: string[] = [];
    this.weatherData.forEach(weatherData => {
      temps.push(weatherData.temp);
      labels.push(weatherData.dt);
    });
    this.lineChartData.push({data: temps, label: 'Hourly Forecast'});
    this.lineChartLabels = labels;
  }
}
