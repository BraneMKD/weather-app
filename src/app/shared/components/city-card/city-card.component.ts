import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { OpenWeatherApiService } from 'src/app/core/services/open-weather-api.service';
import { SnackBarService } from 'src/app/core/services/snack-bar.service';
import { HourlyDialogComponent } from '../../components/hourly-dialog/hourly-dialog.component';
import { CityHourlyForecast, CityWeatherData } from '../../interfaces/weather-data';

/**
 * Component used to display weather data for city
 */
@Component({
  selector: 'app-city-card',
  templateUrl: './city-card.component.html',
  styleUrls: ['./city-card.component.scss']
})
export class CityCardComponent implements OnInit {

  @Input() weatherData!: CityWeatherData;   // Weather data to display city weather

  constructor(public dialog: MatDialog, private weatherApi: OpenWeatherApiService, private snackBarService: SnackBarService) {}

  ngOnInit(): void {}

  /**
   * Function to fetch hourly data for current city
   */
  public showHourly(): void {
    this.weatherApi.getHourlyForecast(this.weatherData.coord.lat, this.weatherData.coord.lon)
      .subscribe((response: CityHourlyForecast[]) => {
        const dialogRef = this.dialog.open(HourlyDialogComponent, {
          data: {
            fullData: this.weatherData,
            hourlyData: response
          }
        });
        dialogRef.afterClosed().subscribe(result => {
          console.log(`Dialog result: ${result}`);
        });
      }, (error) => {
        this.snackBarService.openSnackBar('Failed to load data for ' + this.weatherData.name, 'Close', 2000);
      });
  }
}
