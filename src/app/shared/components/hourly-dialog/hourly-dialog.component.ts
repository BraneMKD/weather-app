import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CityHourlyForecast, CityWeatherData } from '../../interfaces/weather-data';

/**
 * Dialog component used to display chart data for hourly weather for city
 */
@Component({
  selector: 'app-hourly-dialog',
  templateUrl: './hourly-dialog.component.html',
  styleUrls: ['./hourly-dialog.component.scss']
})
export class HourlyDialogComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: { fullData: CityWeatherData, hourlyData: CityHourlyForecast[] }) { }

  ngOnInit(): void {}

}
