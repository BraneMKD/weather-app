import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSnackBarModule } from '@angular/material/snack-bar';

import { SharedRoutingModule } from './shared-routing.module';

import { CityCardComponent } from './components/city-card/city-card.component';
import { HourlyDialogComponent } from './components/hourly-dialog/hourly-dialog.component';
import { ChartsModule } from 'ng2-charts';
import { ChartWrapperComponent } from './components/chart-wrapper/chart-wrapper.component';


@NgModule({
  declarations: [
    CityCardComponent,
    HourlyDialogComponent,
    ChartWrapperComponent
  ],
  imports: [
    CommonModule,
    SharedRoutingModule,
    MatGridListModule,
    MatIconModule,
    MatButtonModule,
    MatDialogModule,
    MatSnackBarModule,
    ChartsModule
  ],
  exports: [
    MatGridListModule,
    CityCardComponent,
    MatIconModule,
    MatButtonModule,
    MatDialogModule,
    MatSnackBarModule,
    ChartsModule,
  ]
})
export class SharedModule { }
