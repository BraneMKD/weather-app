import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

/**
 * Service to display snackbar notifications
 */
@Injectable({
  providedIn: 'root'
})
export class SnackBarService {

  constructor(private snackBar: MatSnackBar) { }

  /**
   * Function to open snackbar notification
   * @param message message to be display
   * @param action action message
   * @param duration duration of the snackbar
   */
  public openSnackBar(message: string, action: string, duration: number): void {
    this.snackBar.open(message, action, {
      duration,
    });
  }
}
