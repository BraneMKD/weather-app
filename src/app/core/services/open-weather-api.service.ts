import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { CityHourlyForecast, CityWeatherData } from 'src/app/shared/interfaces/weather-data';
import {map} from 'rxjs/operators';
import { environment } from 'src/environments/environment';

/**
 * Simple service used to fetch data from open weaker api
 * https://openweathermap.org/api
 */
@Injectable({
  providedIn: 'root'
})
export class OpenWeatherApiService {

  constructor(private http: HttpClient) { }

  /**
   * Function to fetch weather data for single city
   * @param cityName city name in string
   * @returns CityWeatherData object
   */
  public getCurrentWeather(cityName: string): Observable<CityWeatherData> {
    return this.http.get<CityWeatherData>(environment.API_URL + 'weather', {
      params: {
        appid: environment.API_KEY,
        q: cityName,
        units: 'metric',
      }
    }).pipe(map((response: any) => {
      const modifiedResponse = { icon: environment.API_ICON_URL + response.weather[0].icon + '.png', ...response };
      return modifiedResponse;
    }));
  }

  /**
   * Function to fetch hourly weather data for single city
   * @param lat latitude number
   * @param lon longitude numer
   * @returns array of CityHourlyForecast
   */
  public getHourlyForecast(lat: number, lon: number): Observable<CityHourlyForecast[]> {
    return this.http.get<CityHourlyForecast[]>(environment.API_URL + 'onecall', {
      params: {
        appid: environment.API_KEY,
        lat: lat.toString(),
        lon: lon.toString(),
        units: 'metric',
        exclude: 'minutely,daily,current'
      }
    }).pipe(map((response: any) => {
      return response.hourly.slice(0, 12).map((item: any) => {
        return { dt: this.generateDateTime(item.dt), temp: item.temp };
      });
    }));
  }

  /**
   * Function to convert timestampt to readable hour string
   * @param dt datatime number
   * @returns hour string
   */
  private generateDateTime(dt: number): string {
    return this.addZero(new Date(dt * 1000).getHours()) + ':' +  this.addZero(new Date(dt * 1000).getMinutes());
  }

  /**
   * Function to add `0` to hour because JavaScript functions only retuns single digit for values < 10
   * @param i number value
   * @returns adjusted string
   */
  private addZero(i: any): string {
    return i < 10 ? '0' + i : i;
  }
}
