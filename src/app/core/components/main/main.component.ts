import { Component, OnInit } from '@angular/core';
import { CityWeatherData } from 'src/app/shared/interfaces/weather-data';
import { OpenWeatherApiService } from '../../services/open-weather-api.service';
import { SnackBarService } from '../../services/snack-bar.service';

/**
 * Main entry component used to load initial display data
 */
@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  private cities: string[] = ['Skopje', 'Kumanovo', 'Tetovo', 'Bitola', 'Veles'];   // Hardcoded cities
  public weatherData: CityWeatherData[] = [];                                       // Weather data array

  constructor(private weatherApi: OpenWeatherApiService, private snackBarService: SnackBarService) { }

  ngOnInit(): void {
    this.fetchWeatherData();
  }

  /**
   * Function to fetch weather data
   * This function will loop thru each hardcoded city, fetch the data and prepare it for display
   */
  private fetchWeatherData(): void {
    this.cities.forEach((name) => {
      this.weatherApi.getCurrentWeather(name).subscribe((data) => {
        this.weatherData.push(data);
      }, (error) => {
        this.snackBarService.openSnackBar('Failed to load data for ' + name, 'Close', 2000);
      });
    });
  }
}
